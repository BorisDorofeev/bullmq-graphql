import { ApolloServer, AuthenticationError } from 'apollo-server';
import { Queues } from './queues';
import { verify } from './jwt';
import schema from './bullSchema';

function getUserFromReq(req) {
    if (!req)
        throw new Error('Request is empty!');

    const token = (req.cookies ? req.cookies.token : undefined) ||
        (req.headers ? req.headers.authorization : undefined);

    if (token) {
        const payload = verify(token);
        if (payload) {
            //TODO: стучаться куда-то за остальным юзером?
            return {
                id: payload.sub,
                name: payload.name
            };
        }
    } else {
        throw new Error('You are currently not logged in!');
    }    
}

const server = new ApolloServer({
    schema,
    context: ({ req }) => {
        let user;
        try {
            user = getUserFromReq(req);            
        } catch (e) {
            throw new AuthenticationError('Authentication failed! ' + e.message);
        }

        return { user, Queues };
    }
});

server.listen(5000).then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});

import { sign, verify } from './jwt';

const token = sign({
    sub: '123',    
    name: 'Boris',
    fingerprint: 'fingerprint-babla'
});

console.log(token);
console.log(verify(token));